/**
 * 
 */
package com.yuyi.lwq.unionpay;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.unionpay.acp.sdk.SDKConfig;

/**
 * 初始化Bean时，加载配置文件
 * @author 李文庆
 * 2019年1月2日 下午5:45:02
 */
@Component
public class UnionPayInitializer implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		SDKConfig.getConfig().loadPropertiesFromSrc();
	}

}
